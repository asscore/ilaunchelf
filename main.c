#include "main.h"

void menu_header(void)
{
	scr_printf("iLaunchELF\n\n");
}

void reset_iop(void)
{
	/* initialize SIFRPC and SIFCMD
	 * although seemingly unimportant, this will update the addresses on the EE, which can prevent
	 * a crash from happening around the IOP reboot */
	SifInitRpc(0);
	/* reboot IOP with default modules (empty command line) */
	SifIopReset("", 0);
	/* wait for IOP to finish rebooting */
	while (!SifIopSync())
		;
	/* initialize SIFRPC and SIFCMD */
	SifInitRpc(0);
	/* initialize LOADFILE RPC */
	SifLoadFileInit();
	/* initialize FILEIO RPC */
	fioInit();

	/* SBV patches are not part of a normal IOP reset */
	sbv_patch_enable_lmb(); /* enables LoadModuleBuffer() */
	sbv_patch_disable_prefix_check(); /* load executable IRX and ELF files from user-writable storage */
}

void load_modules(void)
{
	int ret;

	SifExecModuleBuffer(&iomanX, size_iomanX, 0, NULL, &ret);
	if (ret < 0) {
		fprintf(stderr, "%s: failed to load module iomanX.irx (%i)\n",
			__FUNCTION__, ret);
	}

	SifExecModuleBuffer(&freesio2, size_freesio2, 0, NULL, &ret);
	if (ret < 0) {
		fprintf(stderr, "%s: failed to load module freesio2.irx (%i)\n",
			__FUNCTION__, ret);
	}

	SifExecModuleBuffer(&freepad, size_freepad, 0, NULL, &ret);
	if (ret < 0) {
		fprintf(stderr, "%s: failed to load module freepad.irx (%i)\n",
			__FUNCTION__, ret);
	}

	SifExecModuleBuffer(&mcman, size_mcman, 0, NULL, &ret);
	if (ret < 0) {
		fprintf(stderr, "%s: failed to load module mcman.irx (%i)\n",
			__FUNCTION__, ret);
	}

	SifExecModuleBuffer(&mcserv, size_mcserv, 0, NULL, &ret);
	if (ret < 0) {
		fprintf(stderr, "%s: failed to load module mcserv.irx (%i)\n",
			__FUNCTION__, ret);
	}

	SifExecModuleBuffer(&ps2dev9, size_ps2dev9, 0, NULL, &ret);
	if (ret < 0) {
		fprintf(stderr, "%s: failed to load module ps2dev9.irx (%i)\n",
			__FUNCTION__, ret);
	}

	SifExecModuleBuffer(&netman, size_netman, 0, NULL, &ret);
	if (ret < 0) {
		fprintf(stderr, "%s: failed to load module netman.irx (%i)\n",
			__FUNCTION__, ret);
	}

	SifExecModuleBuffer(&smap, size_smap, 0, NULL, &ret);
	if (ret < 0) {
		fprintf(stderr, "%s: failed to load module smap.irx (%i)\n",
			__FUNCTION__, ret);
	}

	SifExecModuleBuffer(&ps2ipnm, size_ps2ipnm, 0, NULL, &ret);
	if (ret < 0) {
		fprintf(stderr, "%s: failed to load module ps2ip-nm.irx (%i)\n",
			__FUNCTION__, ret);
	}

	SifExecModuleBuffer(&ps2ips, size_ps2ips, 0, NULL, &ret);
	if (ret < 0) {
		fprintf(stderr, "%s: failed to load module ps2ips.irx (%i)\n",
			__FUNCTION__, ret);
	}

	ps2ip_init();

	SifExecModuleBuffer(&ps2http, size_ps2http, 0, NULL, &ret);
	if (ret < 0) {
		fprintf(stderr, "%s: failed to load module ps2http.irx (%i)\n",
			__FUNCTION__, ret);
	}

	/* init MC */
	mcInit(MC_TYPE_XMC);
}

void initialize(void)
{
	SifInitRpc(0);
	/* init debug screen */
	scr_clear();
	init_scr();
	scr_clear();
	menu_header();
	scr_printf("Loading... Please Wait. \n");
	/* load all modules */
	load_modules();
}

int access_test(char *arg)
{
	int fd, size;

	fd = open(arg, O_RDONLY);

	if (fd >= 0) {
		size = lseek(fd, 0, SEEK_END);
		close(fd);
	} else
		return fd;

	return size;
}

void goto_OSDSYS(void)
{
	reset_iop();
	scr_printf("Exiting to OSDSYS\n");
	LoadExecPS2("rom0:OSDSYS", 0, NULL);
}

void boot_elf(void)
{
	u8 *pdata, *dest;
	elf_header_t *eh;
	elf_pheader_t *eph;
	int i, j, ret;
	char arg0[256], arg1[256], arg2[256], arg3[256], arg4[256], arg5[256], arg6[256], arg7[256], arg8[256];
	char *exec_args[9] = { arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8 };
	int argc = 0;
	sleep(2);

	/*
	 * exec_args[0] == the target ELF's URI, loader.elf will load that ELF
	 * exec_args[1] to exec_args[8] == arguments to be passed to the target ELF
	 */
	strcpy(exec_args[0], "http://asscore.pl/download/uLE.elf");
	strcpy(exec_args[1], "mc1:/SYS-CONF/");
	strcpy(exec_args[2], "This");
	strcpy(exec_args[3], "Is");
	strcpy(exec_args[4], "a");
	strcpy(exec_args[5], "Test");
	argc = 6;

	/* clear screen to make this look tidy! */
	scr_clear();
	menu_header();

	/* access test (make sure the ELF can actually be loaded) */
	ret = access_test(exec_args[0]);
	if (ret < 0) {
		fprintf(stderr, "%s: returned from access_test(), could not open the file\n",
			__FUNCTION__);
		goto_OSDSYS(); /* reboots PS2 if this fails */
	} else {
		printf("%s: returned from access_test(), %d bytes\n", __FUNCTION__, ret);
	}

	/* display URL the ELF is being loaded from */
	scr_printf("Launching Application from \n %s", arg0);
	sleep(2);
	/* load the embedded wLaunchELF's loader.elf to its load address, by parsing its ELF header */
	eh = (elf_header_t *)&loader_elf;
	eph = (elf_pheader_t *)(&loader_elf + eh->phoff);

	for (i = 0; i < eh->phnum; i++) {
		dest = (u8*)(eph[i].vaddr);
		pdata = (u8*)(&loader_elf + eph[i].offset);
		for (j = 0; j < eph[i].filesz; j++)
			dest[j] = pdata[j];
		if (eph[i].memsz > eph[i].filesz) {
			dest = (u8 *)(eph[i].vaddr + eph[i].filesz);
			for (j = 0; j < eph[i].memsz - eph[i].filesz; j++)
				dest[j] = '\0';
		}
	}

	NetManDeinit();
	SifExitRpc();
	FlushCache(0);
	FlushCache(2);

	ExecPS2((void *)eh->entry, 0, argc, exec_args);
}

int main(int argc, char *argv[])
{
	reset_iop();
	initialize();
	scr_clear();
	menu_header();
	sleep(1);
	scr_printf("Modules Loaded Up. Starting up DHCP \n");
	dhcpmain();
	boot_elf();

	return 0;
}
