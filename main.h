#include <tamtypes.h>
#include <kernel.h>
#include <sifrpc.h>
#include <loadfile.h>
#include <iopcontrol.h>
#include <fileio.h>
#include <sbv_patches.h>
#include <libmc.h>
#include <netman.h>
#include <ps2ips.h>
#include <debug.h>
#include <string.h>
#include <unistd.h>

typedef struct{
	u32	magic;
	u8 ident[12];
	u16	type;
	u16	machine;
	u32	version;
	u32	entry;
	u32	phoff;
	u32	shoff;
	u32	flags;
	u16	ehsize;
	u16	phentsize;
	u16	phnum;
	u16	shentsize;
	u16	shnum;
	u16	shstrndx;
} elf_header_t;

typedef struct{
	u32	type;
	u32	offset;
	void *vaddr;
	u32	paddr;
	u32	filesz;
	u32	memsz;
	u32	flags;
	u32	align;
} elf_pheader_t;

/* ps2ip.c */
int dhcpmain(void);

extern void loader_elf; /* wLaunchELF's loader.elf. Embed this sukka in your ELF. */

extern void freesio2;
extern void iomanX;
extern void freepad;
extern void mcman;
extern void mcserv;
extern void ps2dev9;
extern void netman;
extern void smap;
extern void ps2ipnm;
extern void ps2ips;
extern void ps2http;

extern u32 size_freesio2;
extern u32 size_iomanX;
extern u32 size_freepad;
extern u32 size_mcman;
extern u32 size_mcserv;
extern u32 size_ps2dev9;
extern u32 size_netman;
extern u32 size_smap;
extern u32 size_ps2ipnm;
extern u32 size_ps2ips;
extern u32 size_ps2http;
